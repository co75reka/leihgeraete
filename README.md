# Leihgeräte

Dient zum übersichtlichen Tracking der verliehenen Leihgeräte an der RRZE Theke.

## Was ist das Problem mit dem Exchange-Kalender?

Momentan wird für jedes leihbare Gerät ein eigener Kalender unter http://groupware.fau.de/ gepflegt.

### Probleme:

* Limit an gleichzeitig dargestellten Kalendern in der Übersicht (höchstens 10). Wir haben deutlich mehr Leihgeräte
* Zu schmale Seitenleiste, Nummern der Geräte sind abgeschnitten
* Es gibt nur eine Monatsdarstellung, keine Eventlistendarstellung
    * Erfolgte Rückgabe kann nicht schnell überprüft werden, da zum letzten Verleih des jeweiligen Gerätes zurückgeblättert werden muss

Veraltete Informationen können so nur schwer ausgeschlossen werden. Darum soll dieses Board mittelfristig als Ersatz für Exchange Kalender zum Tracken dienen:

https://gitlab.rrze.fau.de/co75reka/leihgeraete/-/boards  (Location des Repos sollte unter der Helpdesk Gruppe sein, die Mitarbeiter an der Theke sollten sowieso dazu einen Zugang bekommen)